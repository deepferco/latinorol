In the sample audio.ini file, only the last two mapped files under
the section labeled "other_pack" (example.ogg and and example.wav)
need to be in this folder.

The first mapped file (http://www.website.com/file.mp3), however, is
a remote file, and it does not need to be in this folder. It only
needs to be on a web server so that clients can download it.